import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    vCenterWrapper: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#33ccff',
    },
    vTopWrapper: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#33ccff',
    },
    scrollWrapper: {
        alignItems: 'center',
        backgroundColor: '#33ccff',
        flex: 1,
    },
    wrapperWidth: {
        width: 300,
    },
    flex1: {
        flex: 1,
    },
    nav: {
        backgroundColor: "#FFFFFF",
        alignSelf: 'stretch',
        height: 50,
        paddingTop: 24
    },
    viewWrapperVCenter: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {   
        width: 300,
        height: 160,
        resizeMode: 'contain',
    },
    menuInput: {
        backgroundColor: 'white',
        width:300,
        height: 50,
        marginBottom: 16,
        textAlign: 'center',
        color: 'black',
        fontSize: 16,
    },
    menuBtn: {
        backgroundColor: '#ff5722',
        height: 50,
        width:300,
    },
    menuBtnTxt: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
    },
    forgotPasswordHeader: {
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'white',
        marginTop:24
    },
    forgotPassword: {
        color: 'white',
        textAlign: 'center',
        marginTop: 16
    },
    marginBottom16: {
        marginBottom: 16
    },
    payBtn: {
        backgroundColor: '#1e88e5',
        height: 50,
        width:140,
    },
});