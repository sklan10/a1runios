import React, { Component } from 'react';
import { AppRegistry, Alert, Button, Image, Navigator, ScrollView, StyleSheet, Text, TextInput, TouchableHighlight, View, 
} from 'react-native';

var BTClient = require('react-native-braintree-xplat');

import styles from '../styles';
import Spinner from './spinner';


class JoinBtnTxt extends Component {
    render(){
        return(
            <Text style={styles.menuBtnTxt}>Join</Text>
        );
    }
}

const options = {
    callToActionText: 'Join'
}

export default class SignUp extends Component {
	constructor(props) {
		super(props);
        this.state = {
        	firstName: '', 
        	lastName: '', 
        	email: '', 
        	password: '',
        	promoCode: '',
        	nonce: '',
        	showSpinner: false,
        	token: '',
        };
        this.backPress = this.backPress.bind(this);
        this.toggleSpinner = this.toggleSpinner.bind(this);
        this.showPaymentScreen = this.showPaymentScreen.bind(this);
	}

    backPress = () => {
        this.props.navigator.push({name: 'LogIn'});
    }

    toggleSpinner = () => {
        this.setState({showSpinner: !this.state.showSpinner});
    }

    showPaymentScreen = () => {
        if ((this.state.firstName == '') || (this.state.lastName == '') || (this.state.email == '') || (this.state.password == '')){
            Alert.alert("Please fill out all required items.");
        }
        else if (!this.state.email.includes("@") || !this.state.email.includes(".")){
            Alert.alert("Please enter a valid email address.");
        }
        else if (this.state.password.length < 5){
            Alert.alert("Password must be at least 5 characters long.");
        }
        else{
            this.toggleSpinner();
            fetch('https://a1run.com/api2/get_braintree_token/')
            .then((response) => response.json())
            .then((responseData) => {
                BTClient.setupWithURLScheme(responseData.token, 'com.ModernMarc.A1RunIos.payments');
                let state = this.state;
                this.toggleSpinner();
                BTClient.showPaymentViewController(options, state).then(function(nonce) {
                    fetch('https://a1run.com/api2/signup/',{
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            firstName: state.firstName, 
                            lastName: state.lastName, 
                            email: state.email, 
                            password: state.password,
                            promoCode: state.promoCode,
                            nonce: nonce,
                        })
                    })
                })
                .then((response) => {
                    console.log(response);
                })
                .catch(function(err) {
                    console.log(err);
                });
            });
        }
    }

	render() {
		return (
            <ScrollView contentContainerStyle={styles.scrollWrapper}>
                <TouchableHighlight style={styles.nav} onPress={this.backPress}>
                    <View style={styles.viewWrapperVCenter}>
                        <Text>Back</Text>
                    </View>
                </TouchableHighlight>
                <View style={styles.wrapperWidth}>
					<Text style={[styles.forgotPasswordHeader]}>Join A1Run!</Text>
					<Text style={[styles.forgotPassword, styles.marginBottom16]}>Only $3.95 a Month!</Text>
					<TextInput placeholder="First Name" style={styles.menuInput} onChangeText={(text) => this.setState({firstName: text})} />
					<TextInput placeholder="Last Name" style={styles.menuInput} onChangeText={(text) => this.setState({lastName: text})} />
					<TextInput placeholder="Email" style={styles.menuInput} onChangeText={(text) => this.setState({email: text})} />
					<TextInput placeholder="Password" style={styles.menuInput} secureTextEntry={true} onChangeText={(text) => this.setState({password: text})} />
					<TextInput placeholder="Promo Code" style={styles.menuInput} onChangeText={(text) => this.setState({promoCode: text})} />
					<TouchableHighlight style={styles.menuBtn} onPress={this.showPaymentScreen}>
                        <View style={styles.viewWrapperVCenter}>
                            { this.state.showSpinner ? <Spinner /> : <JoinBtnTxt /> }
                        </View>
                    </TouchableHighlight>
                    <Text style={[styles.forgotPassword]}>**Your payment method will not be charged until the end of 5-day free trial period. The plan is recurring. Cancel anytime.</Text>
                    <Text style={[styles.forgotPassword]}>By clicking, you agree to A1Run's Terms and Conditions.</Text>
				</View>
			</ScrollView>
		);
	}
}