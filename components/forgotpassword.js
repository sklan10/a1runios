import React, { Component } from 'react';
import { AppRegistry, Alert, Button, Image, Navigator, ScrollView, Text, TextInput, TouchableHighlight, View, 
} from 'react-native';

import styles from '../styles';
import Spinner from './spinner';


class ForgotPasswordBtnTxt extends Component {
    render(){
        return(
            <Text style={styles.menuBtnTxt}>Submit</Text>
        );
    }
}

export default class ForgotPassword extends Component {
	constructor(props) {
		super(props);
        this.state = {email: '', showSpinner: false};
        this.backPress = this.backPress.bind(this);
        this.submitPasswordReset = this.submitPasswordReset.bind(this);
	}

    toggleSpinner = () => {
        this.setState({showSpinner: !this.state.showSpinner});
    }

	submitPasswordReset = () => {
        this.toggleSpinner();
        fetch('https://a1run.com/api2/reset_password/',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.email,
            })
        })
        .then((response) => {
        	this.toggleSpinner();
        	if (response.status == 204) Alert.alert("Check your email to reset your password");
        	else Alert.alert("You will receive a reset email if you have a valid account with A1Run");
        })
        .catch((error) => {
            console.error(error);
        });
	};

    backPress = () => {
        this.props.navigator.push({name: 'LogIn'});
    }

	logInPress = () => {
		this.props.navigator.push({name: 'LogIn'});
	};

    signUpPress = () => {
        this.props.navigator.push({name: 'SignUp'});
    };

	render() {
		return (
            <ScrollView contentContainerStyle={styles.scrollWrapper}>
                <TouchableHighlight style={styles.nav} onPress={this.backPress}>
                    <View style={styles.viewWrapperVCenter}>
                        <Text>Back</Text>
                    </View>
                </TouchableHighlight>
                <View style={styles.wrapperWidth}>
                	<View>
						<Text style={styles.forgotPasswordHeader}>Password Reset</Text>
						<Text style={[styles.forgotPassword, styles.marginBottom16]}>Please submit your email address to receive instructions for resetting your password.</Text>
					</View>
                    <View>
                        <TextInput placeholder="Email Address" style={styles.menuInput} onChangeText={(text) => this.setState({email: text})} />
                    </View>
                    <View>
                        <TouchableHighlight style={styles.menuBtn} onPress={this.submitPasswordReset}>
                            <View style={styles.viewWrapperVCenter}>
                            	{ this.state.showSpinner ? <Spinner /> : <ForgotPasswordBtnTxt /> }    
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View>
                        <Text style={styles.forgotPassword} onPress={this.logInPress}>Already a member? Log In</Text>
                        <Text style={styles.forgotPassword} onPress={this.signUpPress}>Not a member? Sign Up</Text>
                    </View>
				</View>
			</ScrollView>
		);
	}
}