import React, { Component } from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';

import styles from '../styles';

export default class Spinner extends Component {
	render() {
		return (
            <ActivityIndicator color='white' />
		);
	}
}