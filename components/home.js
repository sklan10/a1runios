import React, { Component } from 'react';
import { AppRegistry, Alert, Button, Image, Navigator, StyleSheet, Text, TextInput, TouchableHighlight, View, 
} from 'react-native';

import styles from '../styles';
import Spinner from './spinner';

class Categories extends Component{
    constructor(props) {
        super(props);
    }

    render() {
    	let categories = [];
    	for(let i=0; i<this.props.categories.length; i++){
    		categories.push(this.props.categories[i].title);
    	}
		var categoryList = categories.map(function(category){
			return <text key={category} style={[styles.forgotPasswordHeader]}>{category}</text>;
		})
		console.log(categories, categoryList)
        return <View>{ categoryList }</View>;
    }

}

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {categories: [], showSpinner: true};
        this.toggleSpinner = this.toggleSpinner.bind(this);
    }

    toggleSpinner = () => {
        this.setState({showSpinner: !this.state.showSpinner});
    }

    //Get Workout Categories
	componentDidMount() {
        fetch('https://a1run.com/api2/workouts/',{
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Token ${this.props.token}`,
            },
        })
        .then((response) => response.json())
        .then((responseData) => {
            if (responseData){
            	console.log(responseData);
                this.setState({categories: this.state.categories.concat([responseData])});
                console.log(this.state.categories);
            }
            else{
                Alert.alert("Failed to get categories");
            }
            this.toggleSpinner();
        })
    }

	render() {
		return (
			<View style={styles.vTopWrapper}>
            	{ this.state.showSpinner ? <Spinner /> : <Categories categories={this.state.categories} /> }
            </View>
		);
	}
}