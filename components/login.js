import React, { Component } from 'react';
import { AppRegistry, Alert, Button, Image, Navigator, StyleSheet, Text, TextInput, TouchableHighlight, View, 
} from 'react-native';

import styles from '../styles';
import Spinner from './spinner';


class LogInBtnTxt extends Component {
    render(){
        return(
            <Text style={styles.menuBtnTxt}>Log In</Text>
        );
    }
}


export default class LogIn extends Component {
    constructor(props) {
        super(props);
        this.state = {username: '', password: '', showSpinner: false};
        this.logInPress = this.logInPress.bind(this);
        this.setNewToken = this.setNewToken.bind(this);
        this.toggleSpinner = this.toggleSpinner.bind(this);
    }

    setNewToken = (value) => {
        this.props.onTokenChange(value);
    };

    toggleSpinner = () => {
        this.setState({showSpinner: !this.state.showSpinner});
    }

    //Get Auth Token
    logInPress = () => {
        this.toggleSpinner();
        fetch('https://a1run.com/api_token_auth/',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
            })
        })
        .then((response) => response.json())
        .then((responseData) => {
            if (responseData.token){
                this.toggleSpinner();
                this.setNewToken(responseData.token);
                this.props.navigator.push({name: 'Home'});
            }
            else{
                this.toggleSpinner();
                Alert.alert("Log in failed.\nPlease try again.");
            }
        })
        .catch((error) => {
            console.error(error);
        });
    };

    signUpPress = () => {
        this.props.navigator.push({name: 'SignUp'});
    };

    forgotPassPress = () => {
        this.props.navigator.push({name: 'ForgotPassword'});
    };

    render() {
        return (
            <View style={styles.vCenterWrapper}>
                <View>
                    <Image source={require('../images/logo.png')} style={styles.image} />
                    <TextInput placeholder="Username or Email" style={styles.menuInput} onChangeText={(text) => this.setState({username: text})} />
                    <TextInput placeholder="Password" style={styles.menuInput} secureTextEntry={true} onChangeText={(text) => this.setState({password: text})} />
                    <TouchableHighlight style={styles.menuBtn} onPress={this.logInPress}>
                        <View style={styles.viewWrapperVCenter}>
                            { this.state.showSpinner ? <Spinner /> : <LogInBtnTxt /> }
                        </View>
                    </TouchableHighlight>
                    <Text style={styles.forgotPassword} onPress={this.signUpPress}>Not a member? Sign Up</Text>
                    <Text style={styles.forgotPassword} onPress={this.forgotPassPress}>Forgot your password? Reset it</Text>
                </View>
            </View>
        );
    }
}