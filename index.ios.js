"use strict";

import React, { Component } from 'react';
import { AppRegistry, Alert, Button, Image, Navigator, StyleSheet, Text, TextInput, TouchableHighlight, View, } from 'react-native';

import Home from './components/home';
import LogIn from './components/login';
import SignUp from './components/signup';
import ForgotPassword from './components/forgotpassword';
import styles from './styles';


export default class A1RunIos extends Component {
    constructor(props) {
        super(props);
        this.state = {token: ''};
        this.handleTokenChange = this.handleTokenChange.bind(this);
    }

	renderScene = (route, navigator) => {
		if(route.name == 'LogIn') {
		 	return <LogIn navigator={navigator} onTokenChange={this.handleTokenChange} />
		}
		if(route.name == 'SignUp') {
		 	return <SignUp navigator={navigator} />
		}
		if(route.name == 'ForgotPassword') {
		 	return <ForgotPassword navigator={navigator} />
		}
		if(route.name == 'Home') {
		 	return <Home navigator={navigator} token={this.state.token} />
		}
	};

	handleTokenChange = (value) => {
		this.setState({token: value});
	};

	render() {
		return (
			<Navigator
				initialRoute={{ name: 'LogIn' }}
				renderScene={ this.renderScene }
			/>
		)
	}
}



AppRegistry.registerComponent('A1RunIos', () => A1RunIos);
